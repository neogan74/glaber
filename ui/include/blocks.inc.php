<?php
/*
** Zabbix
** Copyright (C) 2001-2023 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/


require_once dirname(__FILE__).'/graphs.inc.php';
require_once dirname(__FILE__).'/maps.inc.php';
require_once dirname(__FILE__).'/users.inc.php';

function getProblemsByGroupsHosts($groups, $filter_hostids, $filter) {
	
	$filter_ext_ack = array_key_exists('ext_ack', $filter)	? $filter['ext_ack']: EXTACK_OPTION_ALL;
	$filter_evaltype = array_key_exists('evaltype', $filter) ? $filter['evaltype'] : TAG_EVAL_TYPE_AND_OR;
	$filter_tags = array_key_exists('tags', $filter) ? $filter['tags'] : [];
	
	$options = [
		'output' => ['eventid', 'r_eventid', 'objectid', 'severity',  'acknowledged'],
		'groupids' => array_keys($groups),
		'hostids' => $filter_hostids,
		'evaltype' => $filter_evaltype,
		'tags' => $filter_tags,
		'source' => EVENT_SOURCE_TRIGGERS,
		'object' => EVENT_OBJECT_TRIGGER,
		'suppressed' => false,
		'symptom' => false,
		'sortfield' => ['eventid'],
		'sortorder' => ZBX_SORT_DOWN,
		'preservekeys' => true
	];

	if (array_key_exists('severities', $filter)) {
		$filter_severities = implode(',', $filter['severities']);
		$all_severities = implode(',', range(TRIGGER_SEVERITY_NOT_CLASSIFIED, TRIGGER_SEVERITY_COUNT - 1));

		if ($filter_severities !== '' && $filter_severities !== $all_severities) {
			$options['severities'] = $filter['severities'];
		}
	}

	if (array_key_exists('show_suppressed', $filter) && $filter['show_suppressed']) {
		unset($options['suppressed']);
		$options['selectSuppressionData'] = ['maintenanceid', 'suppress_until', 'userid'];
	}

	if ($filter_ext_ack == EXTACK_OPTION_UNACK) {
		$options['acknowledged'] = false;
	}

	if (array_key_exists('problem', $filter) && $filter['problem'] !== '') {
		$options['search'] = ['name' => $filter['problem']];
	}
	
	$problems = API::Problem()->get($options);

	return $problems;
}

function expandGroupsHostsFromFilters(array &$filter_groupids = NULL , array &$filter_hostids = NULL) {
	
	$groups = API::HostGroup()->get([
		'output' => ['groupid', 'name'],
		'groupids' => $filter_groupids,
		'hostids' => $filter_hostids,
		'with_monitored_hosts' => true,
		'preservekeys' => true
	]);
	
	$hosts = API::Host()->get([
		'output' => ['hostid', 'name', 'maintenanceid', 'maintenance_status', 'maintenance_type'],
		'selectHostGroups' => ['groupid'],
		'groupids' => array_keys($groups),
		'hostids' => $filter_hostids,
		'filter' => [
			'maintenance_status' => null
		],
		'monitored_hosts' => true,
		'preservekeys' => true
	]);
	return [$groups, $hosts];
}

function prepareProblemsCountersRequest(array $fields_values) {
		
	$filter_groupids = $fields_values['groupids'] ? getSubGroups($fields_values['groupids']) : null;
	$filter_hostids = $fields_values['hostids'] ?: null;
	$filter_severities = $fields_values['severities'] ?: range(TRIGGER_SEVERITY_NOT_CLASSIFIED,
		TRIGGER_SEVERITY_COUNT - 1
	);

	if ($fields_values['exclude_groupids']) {
		$exclude_groupids = getSubGroups($fields_values['exclude_groupids']);

		if ($filter_hostids === null) {
			// Get all groups if no selected groups defined.
			if ($filter_groupids === null) {
				$filter_groupids = array_keys(API::HostGroup()->get([
					'output' => [],
					'with_hosts' => true,
					'preservekeys' => true
				]));
			}

			$filter_groupids = array_diff($filter_groupids, $exclude_groupids);

			// Get available hosts.
			$filter_hostids = array_keys(API::Host()->get([
				'output' => [],
				'groupids' => $filter_groupids,
				'preservekeys' => true
			]));
		}

		$exclude_hostids = array_keys(API::Host()->get([
			'output' => [],
			'groupids' => $exclude_groupids,
			'preservekeys' => true
		]));

		$filter_hostids = array_diff($filter_hostids, $exclude_hostids);
	}
	return [$filter_groupids, $filter_hostids, $filter_severities];
}

function isFilterdBySeverity(array &$filter_severity, array &$severities) {
	foreach ($filter_severity as $idx => $severity) 
		if (0 < $severities[$severity])
			return false;
	
	return true;
}

function fetchTriggerCountersByHostsAndGroupds(array &$filter_groupids = NULL , array &$filter_hostids = NULL, array &$filter_severities) {
	
	[$groups, $hosts] = expandGroupsHostsFromFilters($filter_groupids, $filter_hostids);
		
	foreach ($groups as $groupid => $group) {
		$groups[$groupid]['highest_severity'] = TRIGGER_SEVERITY_NOT_CLASSIFIED;
		$groups[$groupid]['hosts_total_count'] = 0;
		$groups[$groupid]['hosts_problematic_count'] = 0;
		$groups[$groupid]['hosts_problematic_list'] = [];
		$groups[$groupid]['problems']=[0, 0, 0, 0, 0, 0];
	}

	$trigger_states = CZabbixServer::getHostTriggerCounters(CSessionHelper::getId(), array_keys($hosts));
					
	foreach ($hosts as $host) {
		foreach ($host['hostgroups'] as $group) {
			if (array_key_exists($group['groupid'], $groups)) {
				$groups[$group['groupid']]['hosts_total_count']++;
			}
		}
	}
	
	$hosts_data = [];
	
	foreach ($trigger_states as $idx => $host_stat) {
	
		$hostid = $host_stat['hostid'];
		$host = $hosts[$hostid];
	
		if (isFilterdBySeverity($filter_severities, $host_stat['problems_by_severity'] ))
			continue;
				
		$hosts_data[$host['hostid']] = [
			'host' => $host['name'],
			'hostid' => $host['hostid'],
			'maintenanceid' => $host['maintenanceid'],
			'maintenance_status' => $host['maintenance_status'],
			'maintenance_type' => $host['maintenance_type'],
			'severities' => $host_stat['problems_by_severity']
		];
			
		foreach ($host['hostgroups'] as $group) {
			$groupid = $group['groupid'];
	
			if (!array_key_exists($groupid, $groups)) 
				continue;
				
			$total_problems = array_sum($host_stat['problems_by_severity']);
					
			if (0 < $total_problems) {
				$groups[$groupid]['hosts_problematic_list'][$host['hostid']]['name'] = $host['name'];
				$groups[$groupid]['hosts_problematic_count']++;
			}
					
			foreach ($filter_severities as $idx => $severity)  {
					
				$groups[$groupid]['problems'][$severity] += $host_stat['problems_by_severity'][$severity];
					
				if ( $host_stat['problems_by_severity'][$severity] > 0 && 
						 $severity > $groups[$groupid]['highest_severity'] )
					$groups[$groupid]['highest_severity'] = $severity;
	
			}	
		}
	}
	
	CArrayHelper::sort($groups, [['field' => 'name', 'order' => ZBX_SORT_UP]]);	

	return [$groups, $hosts_data];
}