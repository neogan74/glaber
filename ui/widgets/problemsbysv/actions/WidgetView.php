<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

namespace Widgets\ProblemsBySv\Actions;

use APP, CArrayHelper, CWebUser, CRoleHelper, 
	CControllerDashboardWidgetView,
	CControllerResponseData;

use Widgets\ProblemsBySv\Widget;

require_once APP::getRootDir().'/include/blocks.inc.php';

class WidgetView extends CControllerDashboardWidgetView {

	protected function init(): void {
		parent::init();

		$this->addValidationRules([
			'initial_load' => 'in 0,1'
		]);
	}

	protected function doAction(): void {
		$filter = [
			'groupids' => getSubGroups($this->fields_values['groupids']),
			'exclude_groupids' => getSubGroups($this->fields_values['exclude_groupids']),
			'hostids' => $this->fields_values['hostids'],
			'problem' => $this->fields_values['problem'],
			'severities' => $this->fields_values['severities'],
			'show_type' => $this->fields_values['show_type'],
			'layout' => $this->fields_values['layout'],
			'show_suppressed' => $this->fields_values['show_suppressed'],
			'show_tags' => $this->fields_values['show_tags'],
			'hide_empty_groups' => $this->fields_values['hide_empty_groups'],
			'show_opdata' => $this->fields_values['show_opdata'],
			'ext_ack' => $this->fields_values['ext_ack'],
			'show_timeline' => $this->fields_values['show_timeline'],
			'evaltype' => $this->fields_values['evaltype'],
			'tags' => $this->fields_values['tags']
		];

		$data = $this->getSystemStatusData($filter);

		if ($filter['show_type'] == Widget::SHOW_TOTALS) {
			$data['groups'] = $this->getTotalCounters($data);
		}
		
		$this->setResponse(new CControllerResponseData([
			'name' => $this->getInput('name', $this->widget->getDefaultName()),
			'initial_load' => (bool) $this->getInput('initial_load', 0),
			'data' => $data,
			'filter' => $filter,
			'user' => [
				'debug_mode' => $this->getDebugMode()
			],
			'allowed' => $data['allowed']
		]));
	}

	
/**
 * @param array  $filter
 * @param array  $filter['groupids']           (optional)
 * @param array  $filter['exclude_groupids']   (optional)
 * @param array  $filter['hostids']            (optional)
 * @param string $filter['problem']            (optional)
 * @param array  $filter['severities']         (optional)
 * @param int    $filter['show_suppressed']    (optional)
 * @param int    $filter['hide_empty_groups']  (optional)
 * @param int    $filter['ext_ack']            (optional)
 * @param int    $filter['show_opdata']        (optional)
 *
 * @return array
 */
	protected function getSystemStatusData(array $filter) {

		$show_opdata = array_key_exists('show_opdata', $filter) && $filter['show_opdata'] != OPERATIONAL_DATA_SHOW_NONE;

		[$filter_groupids, $filter_hostids, $filter_severities] = prepareProblemsCountersRequest($filter);
			
		$default_stats = [];

		for ($severity = TRIGGER_SEVERITY_COUNT - 1; $severity >= TRIGGER_SEVERITY_NOT_CLASSIFIED; $severity--) {
			if (in_array($severity, $filter_severities)) {
				$default_stats[$severity] = ['count' => 0, 'eventids' => [], 'count_unack' => 0, 'unack_eventids' => []];
			}
		}

		[$groups, $hosts] = expandGroupsHostsFromFilters($filter_groupids, $filter_hostids);

		foreach ($groups as &$group) {
			$group['stats'] = $default_stats;
			$group['has_problems'] = false;
		}
		unset($group);
	
		$total_stats = $default_stats;

		$hosts_triggers = \CZabbixServer::getHostTriggers(\CSessionHelper::getId(), array_keys($hosts));
		$triggers_to_groups = [];

		foreach ($hosts_triggers as $htidx => $host_triggers )  
			foreach ($host_triggers['triggers'] as $idx => $trigger ) 
				foreach ($hosts[$host_triggers['hostid']]['hostgroups'] as $igrid => $group) 
					$triggers_to_groups[$trigger][] = $group['groupid'];
		
		$problems = getProblemsByGroupsHosts($groups, $filter_hostids, $filter);
	
		foreach ($problems as $idx=> $problem) {

			$triggerid = $problem['objectid'];
			$severity = $problem['severity'];
		
			if (!isset($triggers_to_groups[$triggerid])) 
				continue;
			
			foreach($triggers_to_groups[$triggerid] as $tidx => $groupid)
			{
				if (!isset($groups[$groupid]))
					continue;

				if (!isset($groups[$groupid]['stats'])) {
					$groups[$groupid]['stats'] = $default_stats;
					$groups[$groupid]['has_problems'] = false;	
				}			
	
				$total_stats[$severity]['count']++;
	
				$groups[$groupid]['stats'][$severity]['count']++;
				$groups[$groupid]['has_problems'] = true;
				$groups[$groupid]['stats'][$severity]['eventids'][] = $problem['eventid'];

				if ($problem['acknowledged'] == EVENT_NOT_ACKNOWLEDGED) {
					$total_stats[$severity]['count_unack']++;
					$groups[$groupid]['stats'][$severity]['count_unack']++;
					$groups[$groupid]['stats'][$severity]['unack_eventids'][] = $problem['eventid'];
				}
			}
		}

		CArrayHelper::sort($groups, [['field' => 'name', 'order' => ZBX_SORT_UP]]);

		$data = [
			'groups' => &$groups,
			'triggers' => [],
			'actions' => [],
			'stats' => $total_stats,
			'allowed' => [
		 		'ui_problems' => CWebUser::checkAccess(CRoleHelper::UI_MONITORING_PROBLEMS),
			]
		];

		return $data;
	}


/**
 * @param array  $data
 * @param array  $data['groups']
 * @param string $data['groups'][]['groupid']
 * @param string $data['groups'][]['name']
 * @param bool   $data['groups'][]['has_problems']
 * @param array  $data['groups'][]['stats']
 * @param int    $data['groups'][]['stats'][]['count']
 * @param array  $data['groups'][]['stats'][]['problems']
 * @param int    $data['groups'][]['stats'][]['count_unack']
 * @param array  $data['groups'][]['stats'][]['problems_unack']
 *
 * @return array
 */
	private function getTotalCounters(array $data) {
		$groups_totals = [
			0 => [
				'groupid' => 0,
				'stats' => []
			]
		];

		foreach ($data['stats'] as $severity => $value) {
			$groups_totals[0]['stats'][$severity] = [
				'count' => $value['count'],
				'problems' => [],
				'count_unack' => $value['count_unack'],
				'problems_unack' => []
			];
		}

		foreach ($data['groups'] as $group) {
			foreach ($group['stats'] as $severity => $stat) {
			
				foreach ($stat['eventids'] as $eventid) {
					$groups_totals[0]['stats'][$severity]['eventids'][]= $eventid;
				}
				foreach ($stat['unack_eventids'] as $eventid) {
					$groups_totals[0]['stats'][$severity]['unack_eventids'][]= $eventid;
				}
			}
		}

		return $groups_totals;
	}
}
