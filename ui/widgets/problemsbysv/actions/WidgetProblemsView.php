<?php declare(strict_types = 0);
/*
** Zabbix
** Copyright (C) 2001-2023 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Widgets\ProblemsBySv\Actions;

use API, APP, CControllerResponseData,
	CRoleHelper, CWebUser;

require_once APP::getRootDir().'/include/blocks.inc.php';

class WidgetProblemsView extends \CController {
	protected bool $show_suppressed;
	protected $severities;
	protected $tags;
	protected $evaltype;
	protected $show_opdata;
	protected $show_timeline;
	protected $show_tags;

	protected function init() {
		$this->disableCSRFValidation();
	}

	protected function checkPermissions(): bool {
		return $this->getUserType() >= USER_TYPE_ZABBIX_USER;
	}

	protected function checkInput(): bool {
	
		$fields = [
			'eventids' => 'array',
			'show_timeline' => 'in 1,0',
			'show_opdata' => '',
			'show_suppressed' =>  'in 1,0',
			'show_tags' =>  'in 1,0',
			'ext_ack' =>  '',
			'severitites' => '',
			'tags' => '',
			'problem_name' => 'string',
			'evaltype' => '',
		];

		return	 $this->validateInput($fields);
	}

	protected function doAction(): void {
	
		$eventids = $this->getInput('eventids');

		$this->show_suppressed = $this->hasInput('show_suppressed')? $this->getInput('show_suppressed') : false;
		$this->severities = $this->hasInput('severities')? $this->getInput('severitites') : null ;
		$this->tags = $this->hasInput('tags')? $this->getInput('tags') : null ;
		$this->evaltype = $this->hasInput('evaltype')? $this->getInput('evaltype') : null ;
		$this->ext_ack = $this->hasInput('ext_ack')? $this->getInput('ext_ack') : null ;
		$this->problem_name = $this->hasInput('problem_name')? $this->getInput('problem_name') : null ;
		$this->show_opdata = $this->hasInput('show_opdata')? $this->getInput('show_opdata') : null ;
		$this->show_tags = $this->hasInput('show_tags')? $this->getInput('show_tags') : null ;
		$this->show_timeline =  $this->hasInput('show_timeline')? $this->getInput('show_timeline') : null;
		$this->show_timeline = $this->getInput('show_timeline');
		
		$problems = $this->fetchProblemsByEventids($eventids);
		$triggers = $this->fetchTriggers($problems);

		$actions = getEventsActionsIconsData($problems, $triggers);
		
		if ($this->show_opdata != OPERATIONAL_DATA_SHOW_NONE) {
			$maked_data = \CScreenProblem::makeData(
				['problems' => $problems, 'triggers' => $triggers],
				['show' => 0, 'details' => 0, 'show_opdata' => $this->show_opdata]
			);
			$triggers = $maked_data['triggers'];
		}

		$this->setResponse(new CControllerResponseData([
			'name' => "The list of the hosts",
			'problems' =>  $problems,
			'triggers' =>  $triggers,
			'actions' => [
				'all_actions' => $actions['data'],
		    	'users' => API::User()->get([
		    		'output' => ['username', 'name', 'surname'],
		    		'userids' => array_keys($actions['userids']),
		    		'preservekeys' => true
		    		])
		    ],
			'filters' => [
				'show_timeline' => $this->show_timeline,
				'show_opdata' =>  $this->show_opdata,
				'show_tags' =>  $this->show_tags,
				
			],
			'allowed' => [
				'ui_problems' => CWebUser::checkAccess(CRoleHelper::UI_MONITORING_PROBLEMS),
				'add_comments' => CWebUser::checkAccess(CRoleHelper::ACTIONS_ADD_PROBLEM_COMMENTS),
				'change_severity' => CWebUser::checkAccess(CRoleHelper::ACTIONS_CHANGE_SEVERITY),
				'acknowledge' => CWebUser::checkAccess(CRoleHelper::ACTIONS_ACKNOWLEDGE_PROBLEMS),
				'close' => CWebUser::checkAccess(CRoleHelper::ACTIONS_CLOSE_PROBLEMS),
				'suppress' => CWebUser::checkAccess(CRoleHelper::ACTIONS_SUPPRESS_PROBLEMS)
			]
		]));
	}
	protected function fetchTriggers(&$problems) {
		$triggerids = [];

		foreach ($problems as $problem) {
			$triggerids[$problem['objectid']] = true;
		}

		if (0 == count($triggerids) )
			return [];
		
		$options = [
			'output' => ['priority', 'manual_close'],
			'selectHostGroups' => ['groupid'],
			'selectHosts' => ['name'],
			'triggerids' => array_keys($triggerids),
			'monitored' => true,
			'skipDependent' => true,
			'preservekeys' => true
		];

		if ($this->show_opdata) {
			$options['selectFunctions'] = ['itemid'];
			$options['output'] = array_merge($options['output'],
				['expression', 'recovery_mode', 'recovery_expression', 'opdata']
			);
		}
				
		$triggers = API::Trigger()->get($options);

		if ($this->show_opdata && $triggers) {
			$items = API::Item()->get([
				'output' => ['itemid', 'hostid', 'name', 'key_', 'value_type', 'units'],
				'selectValueMap' => ['mappings'],
				'triggerids' => array_keys($triggers),
				'webitems' => true,
				'preservekeys' => true
			]);

			foreach ($triggers as &$trigger) {
				foreach ($trigger['functions'] as $function) {
					$trigger['items'][] = $items[$function['itemid']];
				}
				unset($trigger['functions']);
			}
			unset($trigger);
		}

		return $triggers;
	}

	protected function fetchProblemsByEventids(array &$eventids) {
		$options = [
			'output' => ['eventid', 'r_eventid', 'objectid', 'clock', 'ns', 'name', 'acknowledged', 'severity'],
			'selectAcknowledges' => ['action', 'clock', 'userid'],
			'eventids' => $eventids,
			'source' => EVENT_SOURCE_TRIGGERS,
			'object' => EVENT_OBJECT_TRIGGER,
			'suppressed' => false,
			'symptom' => false,
			'sortfield' => ['eventid'],
			'sortorder' => ZBX_SORT_DOWN,
			'preservekeys' => true,
			'selectTags' => ['tag', 'value'],
			'selectAcknowledges' => ['userid', 'clock', 'message', 'action', 'old_severity', 'new_severity','suppress_until'],

		];

		if (isset($this->tags)) {
			$options['tags'] = $this->tags;
			$options['evaltype'] = $this->evaltype;
		}

		if (isset($this->severities)) {
			$filter_severities = implode(',', $this->severities);
			$all_severities = implode(',', range(TRIGGER_SEVERITY_NOT_CLASSIFIED, TRIGGER_SEVERITY_COUNT - 1));
	
			if ($filter_severities !== '' && $filter_severities !== $all_severities) {
				$options['severities'] = $this->severities;
			}
		}
	
		if (isset($this->show_suppressed) && $this->show_suppressed) {
			unset($options['suppressed']);
			$options['selectSuppressionData'] = ['maintenanceid', 'suppress_until', 'userid'];
		}
	
		if ($this->ext_ack == EXTACK_OPTION_UNACK) {
			$options['acknowledged'] = false;
		}
	
		if (isset($this->problem_name) && $this->problem_name !== '') {
			$options['search'] = ['name' => $this->problem_name];
		}
	
		$problems = API::Problem()->get($options);
		return $problems;
	}
}
