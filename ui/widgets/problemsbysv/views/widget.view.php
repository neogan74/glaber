<?php declare(strict_types = 0);
/*
** Glaner
** Copyright (C) 2001-2024 Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


/**
 * Problems by severity widget view.
 *
 * @var CView $this
 * @var array $data
 */

use Widgets\ProblemsBySv\Widget;

if ($data['filter']['show_type'] == Widget::SHOW_TOTALS) {
	$table = makeSeverityTotals($data)
		->addClass(ZBX_STYLE_BY_SEVERITY_WIDGET)
		->addClass(ZBX_STYLE_TOTALS_LIST)
		->addClass(($data['filter']['layout'] == STYLE_HORIZONTAL)
			? ZBX_STYLE_TOTALS_LIST_HORIZONTAL
			: ZBX_STYLE_TOTALS_LIST_VERTICAL
		);
}
else {
	$filter_severities = (array_key_exists('severities', $data['filter']) && $data['filter']['severities'])
		? $data['filter']['severities']
		: range(TRIGGER_SEVERITY_NOT_CLASSIFIED, TRIGGER_SEVERITY_COUNT - 1);

	$header = [[_x('Host group', 'compact table header'), (new CSpan())->addClass(ZBX_STYLE_ARROW_UP)]];

	for ($severity = TRIGGER_SEVERITY_COUNT - 1; $severity >= TRIGGER_SEVERITY_NOT_CLASSIFIED; $severity--) {
		if (in_array($severity, $filter_severities)) {
			$header[] = CSeverityHelper::getName($severity);
		}
	}

	$hide_empty_groups = array_key_exists('hide_empty_groups', $data['filter'])
		? $data['filter']['hide_empty_groups']
		: 0;

	$group_url = (new CUrl('zabbix.php'))
		->setArgument('action', 'problem.view')
		->setArgument('filter_set', '1')
		->setArgument('show', TRIGGERS_OPTION_RECENT_PROBLEM)
		->setArgument('hostids', array_key_exists('hostids', $data['filter']) ? $data['filter']['hostids'] : null)
		->setArgument('name', array_key_exists('problem', $data['filter']) ? $data['filter']['problem'] : null)
		->setArgument('show_suppressed',
			(array_key_exists('show_suppressed', $data['filter']) && $data['filter']['show_suppressed'] == 1) ? 1 : null
		);

	$table = makeSeverityTable($data, $hide_empty_groups, $group_url)
		->addClass(ZBX_STYLE_BY_SEVERITY_WIDGET)
		->setHeader($header)
		->setHeadingColumn(0);
}

(new CWidgetView($data))
	->addItem($table)
	->show();



/**
 * @param array $data
 * @param array $data['data']
 * @param array $data['data']['groups']
 * @param array $data['data']['groups'][]['stats']
 * @param array $data['filter']
 * @param array $data['filter']['severities']
 * @param array $data['allowed']
 * @param bool  $data['allowed']['ui_problems']
 * @param bool  $data['allowed']['add_comments']
 * @param bool  $data['allowed']['change_severity']
 * @param bool  $data['allowed']['acknowledge']
 * @param bool  $data['allowed']['close']
 * @param bool  $data['allowed']['suppress']
 * @param bool  $hide_empty_groups
 * @param CUrl  $groupurl
 *
 * @return CTableInfo
 */

 function makeSeverityTable(array $data, $hide_empty_groups = false, CUrl $groupurl = null) {
	$table = new CTableInfo();

	foreach ($data['data']['groups'] as $group) {

		if ($hide_empty_groups && !$group['has_problems']) {
			// Skip row.
			continue;
		}

		if ($data['allowed']['ui_problems']) {
			$groupurl->setArgument('groupids', [$group['groupid']]);
			$row = [new CLink($group['name'], $groupurl->getUrl())];
		}
		else {
			$row = [$group['name']];
		}

		foreach ($group['stats'] as $severity => $stat) {
			if ($data['filter']['severities'] && !in_array($severity, $data['filter']['severities'])) {
				// Skip cell.
				continue;
			}

			$row[] = getSeverityTableCell($severity, $data, $stat);
		}

		$table->addRow($row);
	}

	return $table;
}


/**
 * @param array $data
 * @param array $data['data']
 * @param array $data['data']['groups']
 * @param array $data['data']['groups'][]['stats']
 * @param array $data['filter']
 * @param array $data['filter']['severities']
 * @param array $data['allowed']
 * @param bool  $data['allowed']['ui_problems']
 * @param bool  $data['allowed']['add_comments']
 * @param bool  $data['allowed']['change_severity']
 * @param bool  $data['allowed']['acknowledge']
 * @param bool  $data['allowed']['close']
 * @param bool  $data['allowed']['suppress']
 *
 * @return CDiv
 */
function makeSeverityTotals(array $data) {
	$table = new CDiv();

	foreach ($data['data']['groups'] as $group) {
		foreach ($group['stats'] as $severity => $stat) {
			if ($data['filter']['severities'] && !in_array($severity, $data['filter']['severities'])) {
				// Skip cell.
				continue;
			}
			$table->addItem(getSeverityTableCell($severity, $data, $stat, true));
		}
	}

	return $table;
}

/**
 * @param int   $severity
 * @param array $data
 * @param array $data['data']
 * @param array $data['data']['triggers']
 * @param array $data['data']['actions']
 * @param array $data['filter']
 * @param array $data['filter']['ext_ack']
 * @param array $data['severity_names']
 * @param array $data['allowed']
 * @param bool  $data['allowed']['ui_problems']
 * @param bool  $data['allowed']['add_comments']
 * @param bool  $data['allowed']['change_severity']
 * @param bool  $data['allowed']['acknowledge']
 * @param bool  $data['allowed']['close']
 * @param bool  $data['allowed']['suppress']
 * @param array $stat
 * @param int   $stat['count']
 * @param array $stat['problems']
 * @param int   $stat['count_unack']
 * @param array $stat['problems_unack']
 * @param bool  $is_total
 *
 * @return CCol|string
 */
function getSeverityTableCell($severity, array $data, array $stat, $is_total = false) {
	if (!$is_total && $stat['count'] == 0 && $stat['count_unack'] == 0) {
		return '';
	}

	$severity_name = $is_total ? ' '.CSeverityHelper::getName($severity) : '';
	$ext_ack = array_key_exists('ext_ack', $data['filter']) ? $data['filter']['ext_ack'] : EXTACK_OPTION_ALL;

	$allTriggersNum = $stat['count'];
	if ($allTriggersNum) {

		$data['filter']['eventids'] = $stat['eventids'];
	
		$allTriggersNum = (new CLinkAction($allTriggersNum))
		 	->onClick(
			'return PopUp("widget.problemsbysv.showproblems", '.
				json_encode($data['filter']).
				',{dialogue_class: "modal-popup-xlarge", dialogueid: "show_group_problems"}
			);');
	}

	$unackTriggersNum = $stat['count_unack'];
	if ($unackTriggersNum) {

		$data['filter']['eventids'] = $stat['unack_eventids'];
		$unackTriggersNum = (new CLinkAction($unackTriggersNum))
			->onClick(
			'return PopUp("widget.problemsbysv.showproblems", '.
				json_encode($data['filter']).
				',{dialogue_class: "modal-popup-xlarge", dialogueid: "show_group_problems"});');
	}

	switch ($ext_ack) {
		case EXTACK_OPTION_ALL:
			return CSeverityHelper::makeSeverityCell($severity, [
				(new CSpan($allTriggersNum))->addClass(ZBX_STYLE_TOTALS_LIST_COUNT),
				$severity_name
			], false, $is_total);

		case EXTACK_OPTION_UNACK:
			return CSeverityHelper::makeSeverityCell($severity, [
				(new CSpan($unackTriggersNum))->addClass(ZBX_STYLE_TOTALS_LIST_COUNT),
				$severity_name
			], false, $is_total);

		case EXTACK_OPTION_BOTH:
			return CSeverityHelper::makeSeverityCell($severity, [
				(new CSpan([$unackTriggersNum, ' '._('of').' ', $allTriggersNum]))
					->addClass(ZBX_STYLE_TOTALS_LIST_COUNT),
				$severity_name
			], false, $is_total);

		default:
			return '';
	}
}
