<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

namespace Widgets\ProblemHosts\Actions;

use APP, API,
	CArrayHelper,
	CControllerDashboardWidgetView,
	CControllerResponseData,
	CRoleHelper;

require_once APP::getRootDir().'/include/blocks.inc.php';

class WidgetView extends CControllerDashboardWidgetView {

	protected function doAction(): void {
		
		[$filter_groupids, $filter_hostids, $filter_severities] = \prepareProblemsCountersRequest($this->fields_values);

		[$groups, $hosts_data] = \fetchTriggerCountersByHostsAndGroupds($filter_groupids, $filter_hostids, $filter_severities);
		
		$this->setResponse(new CControllerResponseData([
			'name' => $this->getInput('name', $this->widget->getDefaultName()),
			'filter' => [
				'hostids' => $this->fields_values['hostids'],
				'severities' => $filter_severities,
				'hide_empty_groups' => $this->fields_values['hide_empty_groups'],
				'compact_counters' => $this->fields_values['compact_counters'],
			],
			'hosts_data' => $hosts_data,
			'groups' => $groups,
			'user' => [
				'debug_mode' => $this->getDebugMode()
			],
			'allowed_ui_problems' => $this->checkAccess(CRoleHelper::UI_MONITORING_PROBLEMS)
		]));
	}
	
	
}
