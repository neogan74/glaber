<?php declare(strict_types = 0);
/*
** Zabbix
** Copyright (C) 2001-2023 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


/**
 * Problem hosts widget view.
 *
 * @var CView $this
 * @var array $data
 */

// indicator of sort field
$sort_div = (new CSpan())->addClass(ZBX_STYLE_ARROW_UP);

if ($data['filter']['compact_counters'])
	$problem_header_name = "Problems on hosts";
else 
	$problem_header_name = "Hosts with problems";

$table = (new CTableInfo())
	->setHeader([
		[_x('Host group', 'compact table header'), $sort_div],
		_x($problem_header_name, 'compact table header'),
		_x('Without problems', 'compact table header'),
		//_x('Problems', 'compact table header')
	])
	->setHeadingColumn(0);

$url_group = $data['allowed_ui_problems']
	?  (new CUrl('zabbix.php'))
		->setArgument('action', 'problem.view')
		->setArgument('filter_set', '1')
		->setArgument('show', TRIGGERS_OPTION_RECENT_PROBLEM)
		->setArgument('hostids', $data['filter']['hostids'])
	: null;

foreach ($data['groups'] as $group) {

	$problematic_count_key = 'hosts_problematic_count';

	if ($data['filter']['hide_empty_groups'] && $group[$problematic_count_key] == 0) {
		continue;
	}

	if ($data['allowed_ui_problems']) {
		$url_group->setArgument('groupids', [$group['groupid']]);
		$group_row = [new CLink($group['name'], $url_group->getUrl())];
	}
	else {
		$group_row = [$group['name']];
	}

	if ( 0  < $group['hosts_problematic_count'] ) {
		if ($data['filter']['compact_counters']) {
			$group_content = new CProblemsList($group['problems']);
		} else 
			$group_content = $group['hosts_problematic_count'];

		$col = (new CCol([(new CLinkAction($group_content))
			->onClick(
				'return PopUp("widget.problemhosts.showhosts", '.
					json_encode(['groupid' => $group['groupid'], 
								 'hostids' => array_keys($group['hosts_problematic_list']),
								 'compact_counters' => $data['filter']['compact_counters'],
								 'severities' => $data['filter']['severities']]).',
					{dialogue_class: "modal-popup-medium", dialogueid: "show_hosts_problems_stats"}
				);')
			->setAttribute('aria-label', _xs('%1$s, Severity, %2$s', 'screen reader',
					$group['hosts_problematic_count'], CSeverityHelper::getName((int) $group['highest_severity'])))]));
		
		if (! $data['filter']['compact_counters'])
			$col ->addClass(CSeverityHelper::getStyle((int) $group['highest_severity']));		
		
		$group_row[] = $col;

	} else {
		$group_row[] = '';
	}

	$ok_hosts = $group['hosts_total_count'] - $group['hosts_problematic_count'];
	$group_row[] =	$ok_hosts > 0 ?(new CCol((new CSpan($ok_hosts))->addClass(ZBX_STYLE_GREEN))): " ";

	$table->addRow($group_row);
}

(new CWidgetView($data))
	->addItem($table)
	->show();
