<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber JSC
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


/**
 * Problem hosts widget view.
 *
 * @var CView $this
 * @var array $data
 */


$extra_style = new CTag ('style', true, '
 .svgmap_highlight:hover  {
	     opacity:80%;
	 }	
 ');


$content = $data['svgmap'];
$setup_controls = '';

if ($data['setup_mode']) {
	$setup_controls = (new CDiv())

		->addItem((new CDiv('The map is in the setup mode'))
			->addClass(ZBX_STYLE_RED))
		->addItem( (new CDiv())
			->addItem((new CForm())
				->addVar('widgetid', $data['widgetid'])
				->addVar('dashboardid', $data['dashboardid'])
				->addVar('setup_mode', '0')
				->addVar('action', 'widget.svgmap.changemode')
				->addItem(new CSubmitButton( _('Switch to view mode')))

			)
		);	
}

(new CWidgetView($data))
  	->addItem($setup_controls)
	->addItem($extra_style)
	->addItem($content)
	->show();
