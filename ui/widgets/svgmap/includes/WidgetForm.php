<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber JSC
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/


namespace Widgets\SvgMap\Includes;



use Zabbix\Widgets\{
	CWidgetField,
	CWidgetForm
};

use Zabbix\Widgets\Fields\{
	CWidgetFieldCheckBox,
	CWidgetFieldSeverities,
	CWidgetFieldTextArea, 
	CWidgetFieldRadioButtonList
};

class WidgetForm extends CWidgetForm {
	public function addFields(): self {
		return $this
			->addField(new CWidgetFieldCheckBox('setup_mode', _('Setup mode')))
			->addField((new CWidgetFieldRadioButtonList('fit_mode', _('SVG fit mode'),[
					SCALE_MODE_AS_IS => _('As is'),
					SCALE_MODE_FIT => _('Fit'),
					SCALE_MODE_FIT_WIDTH => _('Fit to width'),
					SCALE_MODE_STRETCH => _('Stretch')  
				]))->setDefault(SCALE_MODE_AS_IS)
			)
			->addField(new CWidgetFieldTextArea('svgmap', _('SVG Map'), 1024 * 1024))
			->addField(new CWidgetFieldTextArea('actions', _('Actions'), 1024 * 1024))
			->addField(	new CWidgetFieldSeverities('severities', _('Severity')) )
		;
	}
}
