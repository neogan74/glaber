<?php declare(strict_types = 0);
/*
** Zabbix
** Copyright (C) 2001-2023 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Widgets\SvgMap\Actions;

require_once __DIR__ . '/../includes/WidgetFieldUpdater.php';
use WidgetFieldUpdater;

class WidgetChangeMode extends \CController {

	protected function init() {
		$this->disableCSRFValidation();
	}

	protected function checkPermissions(): bool {
		return $this->getUserType() >= USER_TYPE_ZABBIX_USER;
	}

	protected function checkInput(): bool {
        $fields = [
			'dashboardid' => '',
			'widgetid' => '',
            'setup_mode' => '',
            'fit_mode' => ''
		];

		return	 $this->validateInput($fields);
	}

	protected function doAction(): void {
        $raw_input = $this->getRawInput();

        $dashboardid = $raw_input['dashboardid'];
        $widgetid = $raw_input['widgetid'];
        $set_mode = $raw_input['setup_mode'];
        
        try {
            $widget = new WidgetFieldUpdater($dashboardid, $widgetid);
        } catch (Exception $e) {
            $this->setResponse(new CControllerResponseFatal());
            return;
        }
        
        $saved_mode = $widget->GetFieldValue('setup_mode');
        
        if ($saved_mode != $set_mode) {
            $widget->SetFieldValue('setup_mode', 0, '0');
            $widget->Save();
        }
              
        $url = (new \CUrl('zabbix.php'))
            ->setArgument('action', 'dashboard.view')
            ->setArgument('dashboardid', $dashboardid);

        $response = new \CControllerResponseRedirect($url);
        $response->redirect();
	}
}