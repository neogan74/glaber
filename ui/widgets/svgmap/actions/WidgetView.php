<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

namespace Widgets\SvgMap\Actions;
require_once __DIR__ . '/../includes/WidgetFieldUpdater.php';

use APP, API, 
	CArrayHelper, CSeverityHelper,
	CControllerDashboardWidgetView,
	CControllerResponseData,
	CRoleHelper,WidgetFieldUpdater;

require_once APP::getRootDir().'/include/blocks.inc.php';

class WidgetView extends CControllerDashboardWidgetView {
	protected array $ids = [];
	protected $actions;
	protected $template_text;
	protected $macroses;
	protected $severities = [1,1,1,1,1,1];
	protected $fit_mode = SCALE_MODE_AS_IS;  //'width'; //'as_is' , 'fit', 'witdth';

	protected $widgetid;
	protected $dashboardid;
	protected $widget_data;

	protected $hosts = [], $groups = [], $items = [], $triggers = [];

	protected function doAction(): void {
		$this->loadWidgetData();
		
		$iterator_svg = simplexml_load_string($this->loadSvgMap(), "SimpleXMLElement");
		
        $setup_mode = isset($this->widget_data) ? $this->widget_data->GetFieldValue('setup_mode'): 0;
		
		if ($setup_mode) {
			$this->loadActions();
			$this->processSetupSVG($iterator_svg, [], "");
		}
		else {
			$this->loadActions();
			$this->loadObjectStates();
			$this->processViewSVG($iterator_svg, ['rules'=>[], 'macroses'=> []], "");	
		}
		
		$this->setResponse(new CControllerResponseData([
			'name' => $this->getInput('name', $this->widget->getDefaultName()),
			'setup_mode' => $setup_mode,
			'dashboardid' => $this->dashboardid,
			'widgetid' => $this->widgetid,
			'svgmap' => $iterator_svg->asXML(),
		]));
	}

	protected function loadActions() {
		$actions_json = $this->fields_values['actions'];
		$this->actions = json_decode($actions_json, true);
	}

	protected function loadObjectStates() {

		$hostids = []; 
		$itemids = [];
		$groupids = [];
		$triggerids = [];
		
		if (!isset($this->actions))
			return;
		
		foreach ($this->actions as $action_id => $action) {
			if (isset($action['bind_object_type'])){
				$object_type = $action['bind_object_type'];
				if ( 0 == $object_type)
					continue;
				switch ($object_type) {
					case OBJECT_TYPE_HOST:
						if (isset( $action['hostids']) )
							foreach ($action['hostids'] as $hostid)
								$hostids[$hostid]=1;
						break;
					case OBJECT_TYPE_HOSTGROUP:
						if (isset( $action['groupids']) )
							foreach ($action['groupids'] as $groupid)
								$groupids[$groupid] = 1;
						break;
					case OBJECT_TYPE_ITEM:
						if (isset( $action['itemids']) )
							foreach($action['itemids'] as $itemid )
								$itemids[$itemid]=1;
						break;
					case OBJECT_TYPE_TRIGGER:
						if (isset( $action['triggerids']) )
							foreach($action['triggerids'] as $triggerid)
								$triggerids[$triggerid]=1;
						break;
				}
			}
		}

		$this->fetchObjectsAndProblems($hostids, $groupids, $itemids, $triggerids);
	}

	protected function fetchObjectsAndProblems($hostids, $groupids, $itemids, $triggerids) {
		if (count($hostids) > 0 )
			$this->hosts = API::Host()->get(
				[ 	'output' => ['hostid','host','status', 'name', 'description', 'maintenance_status'],
			  		'hostids' => array_keys($hostids),
			  		'preservekeys' => true
				]);
		
		if ( count($groupids) > 0 ) {
			$this->groups = API::HostGroup()->get(
					[ 	'output' => ['groupid','name'],
						'groupids' => array_keys($groupids),
						'preservekeys' => true
					]);
		}

		if (count($itemids) > 0 )
			$this->items = API::Item()->get(
			[ 	'output' => ['itemid', 'snmp_oid', 'hostid', 'name', 'key_', 'status', 'units', 'lastdata', 'lastvalue', 'prevvalue'],
				'itemids' => array_keys($itemids),
				'preservekeys' => true
			]);

		if (count($triggerids) > 0 )
			$this->triggers = API::Trigger()->get(
			[ 	'output' => ['triggerid', 'description', 'status', 'value', 'priority', 'lastchange', 'comments', 'error', 'state'],
				'triggerids' => array_keys($triggerids),
				'preservekeys' => true
			]);

		if ( count($groupids) > 0) {
			$this->hosts +=
				 API::Host()->get([
				'output' => ['hostid','host','status', 'name', 'description'],
				'selectHostGroups' => ['groupid'],
				'groupids' => array_keys($groupids),
				'filter' => [
					'maintenance_status' => null
				],
				'preservekeys' => true
			]);
		}

		if (count($this->hosts) > 0) {
			$hosts_triggers = \CZabbixServer::getHostTriggerCounters(\CSessionHelper::getId(), array_keys($this->hosts));
			
			foreach ($hosts_triggers as $idx => $trigger_cnt) {
				$this->hosts[$trigger_cnt['hostid']] += $trigger_cnt;
			
				//hosts that present in the array due to host selection on the map will have no groups, skipping them
				if (!isset($this->hosts[$trigger_cnt['hostid']]['hostgroups']))
					continue;

				foreach ($this->hosts[$trigger_cnt['hostid']]['hostgroups'] as $idx => $group) {
					$groupid = $group['groupid'];

					if (!isset($this->groups[$groupid]))
						continue;
					
					$this->groups[$groupid]['hosts'][$trigger_cnt['hostid']] = 1;	
				}
			}
		}
	}

	protected function processSetupSVG($element, $context, $stack) {
		
		$childNodes = $element->children();
		$this->processAttrRules($element, $context);
		
		$attrs = $element->attributes();

		if (isset($attrs['id']) && $this->CheckValidIDObjects($element->getName(), $attrs) ) {
			$attr_id = strval($attrs['id']);
			
			
			if ( isset($this->actions[$attr_id]) ) {
				$this->processAttrRules($element, [['attr_name' => 'opacity', 'attr_action' => 'set', 'attr_value' => '30%']]);
			} 
	
			$element->addAttribute('cursor','pointer');
			
			if (isset($attrs['class']))  {
				$attributes['class'] = $attrs['class'] . " ". 'svgmap_highlight' ;
			} else 
				$element->addAttribute('class','svgmap_highlight');
			
			$element->addAttribute('onclick','super_highlight(event, this); PopUp("widget.svgmap.rule.edit.view",
		 											{elementid:"'.$attrs['id'].'",
		 											 dashboardid:"'.$this->dashboardid.'",
		 											 widgetid:"'.$this->widgetid.'"},
		 											{dialogue_class: "modal-popup-generic"}); event.stopPropagation();');
		
		}
		
		foreach ($childNodes as $childNode) 
				$this->processSetupSVG($childNode, $context, $stack);

	}
	
	protected function processViewSVG($element, $context, $stack) {
	
		$this->processAttrRules($element, $context['rules']);
		$matched = NULL;

		$new_content = preg_replace_callback('/\{([A-Z,_]*)\}/',
				function($match) use ($context, &$matched) { 
					if (isset($context['macroses'][$match[1]])) {
						$matched = 1;
						return ($context['macroses'][$match[1]]);
					}
						
					return $match[0];
				}, (string)$element	);

		if ($new_content && $matched ) 
			$element[0] = $new_content;

		$attrs = $element->attributes();
		$childNodes = $element->children();

		if ('svg' == $element->getName() && SCALE_MODE_AS_IS != $this->fit_mode) {
			switch ($this->fit_mode) {
				case SCALE_MODE_FIT:
					$this->processAttrRules($element,[
							['attr_name' => 'width', 'attr_action' => 'set', 'attr_value' => '100%'],
							['attr_name' => 'height', 'attr_action' => 'set', 'attr_value' => '100%']
					]);
					break;
				case SCALE_MODE_FIT_WIDTH:
					$this->processAttrRules($element,[['attr_name' => 'width', 'attr_action' => 'set', 'attr_value' => '100%'],
													  ['attr_name' => 'height', 'attr_action' => 'unset'] ]);
					break;
				case SCALE_MODE_STRETCH: 
					$this->processAttrRules($element,[
						['attr_name' => 'width', 'attr_action' => 'set', 'attr_value' => '100%'],
						['attr_name' => 'height', 'attr_action' => 'set', 'attr_value' => '100%'],
						['attr_name' => 'preserveAspectRatio', 'attr_action' => 'set', 'attr_value' => 'none'] ]);
					break;
			}		
		}

		if (isset($attrs['id']) && $this->checkValidIDObjects($element->getName(), $attrs)) {
			$attr_id = strval($attrs['id']);

			if ( isset($this->actions[$attr_id]) ) {
	 			[$node_rules, $new_context] = $this->processAction($this->actions[$attr_id]);
					
				$this->processAttrRules($element, $node_rules);

				if (isset($macroses['PROBLEMS_TOTAL']) && 0 < $new_context['macroses']['PROBLEMS_TOTAL'])
					$element->addChild('title', _('Problems'). ":". $new_context['macroses']['PROBLEMS_TOTAL']);
		 			
				if (count($childNodes) > 0) {
					foreach ($childNodes as $childNode) 
						$this->processViewSVG($childNode, $new_context, $stack);
				}
					
				return;
			}  
		}			
				
		foreach ($childNodes as $childNode) 
				$this->processViewSVG($childNode, $context, $stack);
	}
	
	protected function CheckValidIDObjects($name, $attrs) {
		if  ('svg' == $name) return false;
		//skipping inkscape's ids
		if ('g' == $name && preg_match('/layer\d+/', $attrs['id'])) return false;
		
		return true;
	}
	
	protected function processAction($action) {
		
		$element_rules = [];
		$context = ['rules' => [], 'macroses' => []];
		
		if (isset($action['bind_object_type']) && $action['bind_object_type'] >0 ) {
			switch ($action['bind_object_type']) {
				case OBJECT_TYPE_HOST:
					if (isset ($action['hostids'] )&& is_array($action['hostids']) &&  count($action['hostids']) > 0 ) {
						$this->setHostsProblemsMacro($action['hostids'], $context['macroses']);

						if (1 == count($action['hostids'])) 
							$this->createHostMacroses($action['hostids'][0], $context['macroses']);
					}

					break;
				case OBJECT_TYPE_HOSTGROUP: 
					if (isset ($action['groupids'] )&& is_array($action['groupids']) &&  count($action['groupids'])> 0 ) {
						$this->setGroupsProblemsMacro($action['groupids'], $context['macroses']);
					
						if (1 == count($action['groupids']))  
							$this->createGroupMacroses($action['groupids'][0], $context['macroses']);
					}

					break;
				case OBJECT_TYPE_ITEM: 
					if (isset ($action['itemids'] )&& is_array($action['itemids']) &&  count($action['itemids'])> 0 ) {
						$this->setItemsProblemsMacro($action['itemids'], $context['macroses']);
					
						if (1 == count($action['itemids']))  
							$this->createItemMacroses($action['itemids'][0], $context['macroses']);
					}
					break;
				case OBJECT_TYPE_TRIGGER:
					if (isset ($action['triggerids'] )&& is_array($action['triggerids']) &&  count($action['triggerids'])> 0 ) {
						$this->setTriggersProblemsMacro($action['triggerids'], $context['macroses']);
					
						if (1 == count($action['triggerids']))  
							$this->createTriggerMacroses($action['triggerids'][0], $context['macroses']);
					}
					break;
			}	

			//view objects binding is here, it works if bind is set and set object menu or problem menu
			if (isset($action['on_click_action_type'])) {
				switch ($action['on_click_action_type']) {
					case ONCLICK_ACTION_OBJECT_VIEW:
					//	error_log("Binding objects popup to the graph element\n\n");
						$this->addElementObjectInfoPopup($element_rules, $action);
						break;
					case ONCLICK_ACTION_PROBLEMS_LIST:
					//	error_log("Binding problems popup to the graph element");
						break;
				}
			}

							
			if (isset($context['macroses']['MAXSEVERITY_COLOR']) && '0' != $context['macroses']['MAXSEVERITY_COLOR']) {
				
				$rule =[];

				if (!isset($action['colorize_type']))
					$action['colorize_type'] = COLORIZE_MODE_FILL;

				switch	($action['colorize_type']) {
					case COLORIZE_MODE_FILL:		
						$rule =   [ 'attr_name' => 'fill', 'attr_value' => '#'.$context['macroses']['MAXSEVERITY_COLOR'], 
									'attr_action' => 'replace_fill'];
						break;
					case COLORIZE_MODE_OUTLINE:
						$rule =   [ 'attr_name' => 'stroke', 'attr_value' => '#'.$context['macroses']['MAXSEVERITY_COLOR'], 
									'attr_action' => 'replace'];
						break;
					case COLORIZE_MODE_TEXT:
						$rule =   [ 'attr_name' => 'color', 'attr_value' => '#'.$context['macroses']['MAXSEVERITY_COLOR'], 
									'attr_action' => 'replace'];
						break;
				}
				
				if (count($rule) > 0) {
					$element_rules[] = $rule;
					$context['rules'][] = $rule;
				}
			}
		}


		if ( ((isset($action['on_click_action_type']) &&  $action['on_click_action_type'] == ONCLICK_ACTION_URL_MENU ) ||
			  																!isset($action['on_click_action_type']) )	
			&& isset($action['urls'])) {
				$element_rules[]= ['attr_name' => 'class', 'attr_action' => 'set', 'attr_value' => 'svgmap_highlight'];
				$element_rules[] =['attr_name' => 'cursor', 'attr_action' => 'set', 'attr_value' => 'pointer'];
				$url_items = [];
				
				foreach ($action['urls'] as $link) {
					if (isset($link['name']) && strlen($link['name'] > 0 && isset($link['url']) && strlen($link['url'] > 0)))
						$url_items['items'][] = ['label'=> $link['name'], 'url'=> $link['url']];
				}

				$element_rules[] =['attr_name' => 'data-menu-popup', 'attr_action' => 'set', 'attr_value' => 
										'{"type":"custom", "data":'.json_encode($url_items).'}'];
		}
		
		
		return [$element_rules, $context];
	}
	
	protected function addElementObjectInfoPopup(array &$element_rules, array &$action) {
		$element_rules[] =['attr_name' => 'cursor', 'attr_action' => 'set', 'attr_value' => 'pointer'];
		
		$element_rules[] = ['attr_name' => 'onclick', 'attr_action' => 'set' , 
				 'attr_value' => 'PopUp("items.popup.values.brief", { itemids:'.json_encode($action['itemids']).'},
		 						{dialogue_class: "modal-popup-generic"}); event.stopPropagation();'];
	
	}



	protected function processAttrRules($xml_element, $rules) {
		$attributes = $xml_element->attributes();

		foreach ($rules as $idx => $rule) {
			$attr_name = $rule['attr_name'];

			switch ($rule['attr_action']) {
				case 'replace_fill':
					if (isset($attributes->$attr_name) && 
						strtolower($attributes->$attr_name) != '#000000' &&
						strtolower($attributes->$attr_name) != '#ffffff') {
					 		$attributes->$attr_name = (string)$rule['attr_value'];
						break;
					} 
					//filling might be done via style attribute
					if (isset($attributes['style'])) {
						$new_style = $attributes['style'];
						
						//here is possible future problem: will also match. say,  'refill' attr 
						$new_style= preg_replace('/fill:\s*#\w+/','fill:'.$rule['attr_value'],$new_style);
						$attributes['style'] = $new_style;
						break;
					}
					break;

				case 'replace':

					if (isset($attributes->$attr_name)) 
						$attributes->$attr_name = (string)$rule['attr_value'];
				
					break;
				case 'set':

					if (isset($attributes->$attr_name)) 
						$attributes->$attr_name = $rule['attr_value'];
					else  
						$attributes->addAttribute($attr_name, $rule['attr_value']);
					break;
				case 'unset':
						if (isset($attributes->$attr_name)) 
							$attributes->$attr_name = NULL;
					break;
						
			}
		}
	}
	
	protected function getMaxSeverity(array $problems) {
		for ($i = 5; $i>=0; $i--) {
			if ($problems[$i] > 0 ) 
				return $i;
		}

		return 0;
	}
	protected function getMaxSeverityColor(array $problems) {
		for ($i = 5; $i>=0; $i--) {
			if ($problems[$i] > 0 ) 
				return CSeverityHelper::getColor($i);
		}

		return 0;
	}
	protected function getGroupsHostids(array $groupids) {
		$hostids = [];
		foreach ($groupids as $groupid) 
			if (isset($this->groups[$groupid]['hosts']))
				foreach ($this->groups[$groupid]['hosts'] as $hostid => $val)
					$hostids[$hostid] = 1;

		return array_keys($hostids);
	}
	
	protected function setHostsProblemsMacro(array $hostids, &$macroses) {
		$agg_problems = $this->aggregateHostsProblems($hostids);
		
		
		$macroses['PROBLEMS_TOTAL'] = array_sum($agg_problems);
		$macroses['MAXSEVERITY'] = $this->getMaxSeverity($agg_problems);
		$macroses['MAXSEVERITY_COLOR'] = $this->getMaxSeverityColor($agg_problems);

	}
	

	protected function setGroupsProblemsMacro(array $groupids, &$macroses) {

		$hostids = $this->getGroupsHostids($groupids);
		$agg_problems = $this->aggregateHostsProblems($hostids);
		
		$macroses['PROBLEMS_TOTAL'] = array_sum($agg_problems);
		$macroses['MAXSEVERITY'] = $this->getMaxSeverity($agg_problems);
		$macroses['MAXSEVERITY_COLOR'] = $this->getMaxSeverityColor($agg_problems);
	}

	protected function setItemsProblemsMacro(array $itemids, &$macroses) {
		$agg_problems = $this->getItemsTriggersCounters($itemids);
		$macroses['PROBLEMS_TOTAL'] = array_sum($agg_problems);
		$macroses['MAXSEVERITY'] = $this->getMaxSeverity($agg_problems);
		$macroses['MAXSEVERITY_COLOR'] = $this->getMaxSeverityColor($agg_problems);

		//error_dump_obj("Macroses are:", $macroses);
	}

	protected function setTriggersProblemsMacro(array $itemids, &$macroses) {
		
	}
	
	protected function getItemsTriggersCounters(array $itemids) {
		$problems = [0,0,0,0,0,0];

		$items = API::Item()->get([
			'output' => ['itemid', 'type', 'status', 'state', 'error'],
			//'selectHosts' => ['hostid','host'],
			//'selectTags' => ['tag', 'value'],
			//'selectValueMap' => ['mappings'],
			//'hostids' => array_column($hosts,'hostid'),
            'itemids' => $itemids,
			'webitems' => true,
			'preservekeys' => true,
			//'discovery_items' => true,
			'selectTriggers' => ['triggerid', 'name', 'value', 'priority']
			]);
		foreach ($items as $itemid => $item) {
			foreach ($item['triggers'] as $trigger) 
				if ($trigger['value'] == 1)
				$problems[$trigger['priority']]++;
		}

		return $problems;
	}



	protected function aggregateHostsProblems(array $hostids) {
		$problems = [0,0,0,0,0,0];

		array_unique($hostids);
		
		foreach ($hostids as $hostid) {
			if (isset($this->hosts[$hostid]) && isset($this->hosts[$hostid]['problems_by_severity']))
				foreach($this->hosts[$hostid]['problems_by_severity'] as $severity => $count) 
					if ($this->severities[$severity])
						$problems[$severity] += $count;
		}

		return $problems;
	}

	protected function createHostMacroses($hostid, &$macroses){

		if (!isset($this->hosts[$hostid]))
			return [];
					
		foreach ($this->hosts[$hostid] as $prop => $value) 
			$macroses[strtoupper($prop)] = $value;
	
		if ('0' != $this->hosts[$hostid]['maintenance_status'] || 0 != $this->hosts[$hostid]['status']) {
			$macroses['MAXSEVERITY_COLOR'] = OBJECT_DISABLED_COLOR;
		}
		
	}
	
	protected function createGroupMacroses($groupid, &$macroses){

		if (!isset($this->groups[$groupid]))
			return [];

		foreach ($this->groups[$groupid] as $prop => $value) 
			$macroses[strtoupper($prop)] = $value;
		
	}
	
	protected function createItemMacroses($itemid, &$macroses){
		if (!isset($this->items[$itemid]))
			return [];
	
		foreach ($this->items[$itemid] as $prop => $value) 
			$macroses[strtoupper($prop)] = $value;

		if ('0' != $this->items[$itemid]['status'])
			$macroses['MAXSEVERITY_COLOR'] = OBJECT_DISABLED_COLOR;

		$macroses['RVAL'] = 'Raw value';
		$macroses['VAL'] = 'value';
	}
	
	protected function createTriggerMacroses($triggerid, &$macroses){
		if (!isset($this->triggers[$triggerid]))
			return [];
	
		foreach ($this->triggers[$triggerid] as $prop => $value) 
			$macroses[strtoupper($prop)] = $value;

		if ('0' != $this->triggers[$triggerid]['status'])
			$macroses['MAXSEVERITY_COLOR'] = OBJECT_DISABLED_COLOR;

	}
 
	protected function loadWidgetData() {
		$raw_input = $this->getRawInput();

		if (!isset( $raw_input['widgetid']) )
			return ;

		$this->widgetid = $raw_input['widgetid'];
		$this->dashboardid = $raw_input['dashboardid'];
		
		if (isset($raw_input['fields']['severities'])) {
			$this->severities = [0,0,0,0,0,0];
			if (is_array($raw_input['fields']['severities']))
				foreach ($raw_input['fields']['severities'] as $severity) 
					$this->severities[$severity] = 1;
			else 
				$this->severities[$raw_input['fields']['severities']] = 1;
		}

		if  (isset($raw_input['fields']['fit_mode'])) {
			$this->fit_mode = $raw_input['fields']['fit_mode'];
		}
		

		try {
            $this->widget_data = new WidgetFieldUpdater($this->dashboardid, $this->widgetid);
        } catch (Exception $e) {
            $this->setResponse(new CControllerResponseFatal());
            return;
        } 
	}

	protected function loadSvgMap () {
		return  $this->fields_values['svgmap'];
	}
}
