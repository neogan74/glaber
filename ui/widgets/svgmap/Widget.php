<?php declare(strict_types = 0);
/*
** Zabbix
** Copyright (C) 2001-2023 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/


namespace Widgets\SvgMap;

use Zabbix\Core\CWidget;

define ('OBJECT_DISABLED_COLOR', '707070');

//this are saved in the settings, so IDs aren't supposed to be changed
define('OBJECT_TYPE_HOST',		1);
define('OBJECT_TYPE_HOSTGROUP',	2);
define('OBJECT_TYPE_ITEM',		3);
define('OBJECT_TYPE_TRIGGER',	4);

define('SCALE_MODE_AS_IS', 		1);
define('SCALE_MODE_FIT_WIDTH',	2);
define('SCALE_MODE_FIT', 		3);
define('SCALE_MODE_STRETCH', 	4);

define('COLORIZE_MODE_NONE', 		0);
define('COLORIZE_MODE_FILL', 		1);
define('COLORIZE_MODE_OUTLINE',	2);
define('COLORIZE_MODE_TEXT', 		3);

define('ONCLICK_ACTION_NONE', 0);
define('ONCLICK_ACTION_URL_MENU', 1);
define('ONCLICK_ACTION_OBJECT_VIEW', 2);
define('ONCLICK_ACTION_PROBLEMS_LIST', 3);





class Widget extends CWidget {

	public function getDefaultName(): string {
		return _('SVG map');
	}
}
