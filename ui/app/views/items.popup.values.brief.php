<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber JSC
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

$output = [
	//'header' => '', //$data['item']['name'],
	//'body' => makeItemDetail($data['items'], $data['history']),
	'body' => makeItemsListBrief($data['items'], $data['history']) ,//($data['items'], $data['history']),
    'buttons' => []
];

echo json_encode($output);

function makeItemsListBrief($items, $history) {

	$itemsTable = (new CTableInfo())->setHeader([_('Host'), _('Key'),_('Value')]);

	foreach($items as $itemid => $item)  {

	//	error_dump_obj("Item is ", $item);

		$value = new CLatestValue($items[$itemid] , 
			isset($data['history'][$itemid])? $data['history'][$itemid] : null ,
			isset($item['triggers'])? $item['triggers'] : null,
			1
		);
		$value->calcSeverities();
		$col_value = (new CCol($value));
		
		if ($value->GetWorstSeverity() > 0) 
			$col_value->addClass(CSeverityHelper::getStatusStyle($value->GetWorstSeverity()));

		$item_name = (new CLinkAction($item['name']))
			->setMenuPopup(CMenuPopupHelper::getItem(['itemid' => $item['itemid'], 'backurl' => '', 'context' => 'host']));


		$itemsTable->addRow([new CHostLink($item['hosts'][0]), 
							$item_name,
							$col_value]);
	}

	return (new CDiv(''))
			->addItem($itemsTable)
			->toString();
}