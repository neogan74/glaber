variable "source_qcow" {
  type    = string
  default = "https://cloud.debian.org/images/cloud/bullseye/20231013-1532/debian-11-genericcloud-amd64-20231013-1532.raw"
}

variable "source_checksum_url" {
  type    = string
  default = "file:https://cloud.debian.org/images/cloud/bullseye/20231013-1532/SHA512SUMS"
}

variable "username" {
  type    = string
  default = "debian"
}

variable "password" {
  type    = string
  default = "paSSw0rd"
  sensitive = true
}

variable "output_dir" {
  type    = string
  default = "vm-output"
}

variable "output_name" {
  type    = string
  default = "glaber.qcow2"
}

variable "glaber_db_type" {
  type    = string
  default = "${env("glaber_db_type")}"
}

variable "glaber_build_version" {
  type    = string
  default = "${env("glaber_build_version")}"
}

variable "glaber_workers" {
  type    = string
  default = "${env("glaber_workers")}"
}

variable "glaber_repo" {
  type    = string
  default = "${env("glaber_repo")}"
}
