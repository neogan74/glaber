build {
  name = "Build glaber vm appliance"
  sources = [
    "source.qemu.debian"
  ]

  provisioner "ansible" {
    playbook_file           = "ansible/glaber.yaml"
    ansible_ssh_extra_args  = ["-o IdentitiesOnly=yes"]
    extra_arguments = [
      "-e", "glaber_db_type=${var.glaber_db_type}",
      "-e", "glaber_build_version=${var.glaber_build_version}",
      "-e", "glaber_workers=${var.glaber_workers}",
      "-e", "glaber_repo=${var.glaber_repo}"
    ]
    ansible_env_vars = [
      "ANSIBLE_CONFIG=ansible/ansible.cfg",
      "ANSIBLE_ALLOW_WORLD_READABLE_TMPFILES=true",
      "ANSIBLE_SCP_EXTRA_ARGS=-O"
    ]
    inventory_directory = "ansible/inventory"
    roles_path = "ansible/roles"
  }

}
