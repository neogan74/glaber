#!/bin/bash

# Variables
os="debian_11"
ya_access_key="${YA_ACCESS_KEY}"
ya_secret_key="${YA_SECRET_KEY}"
aws_default_region="ru-central1"
s3_endpoint_url="https://storage.yandexcloud.net"
s3_bucket="glaber-vms"
s3_bucket_path="disks"

# Function to upload QCOW2 image to S3 and update the index.html
upload_to_s3_and_update_index() {
  local db_installation_type="$1"
  local object_name="glaber_${os}_${db_installation_type}_${glaber_build_version}.qcow2"

  # Configure AWS CLI
  export AWS_ACCESS_KEY_ID="${ya_access_key}"
  export AWS_SECRET_ACCESS_KEY="${ya_secret_key}"
  export AWS_DEFAULT_REGION="${aws_default_region}"

  # Upload the QCOW2 image to S3
  aws s3 --endpoint-url="${s3_endpoint_url}" cp "vm-output/glaber.qcow2" "s3://${s3_bucket}/${s3_bucket_path}/${object_name}"

  # Update index.html with the new object
  touch index.html
  objects=$(aws s3 --endpoint-url="${s3_endpoint_url}" ls "s3://${s3_bucket}/${s3_bucket_path}/" | awk '{print $4}')
  for i in ${objects}; do
    echo "<a href='./${s3_bucket_path}/${i}'>${i}</a><br>" >> index.html
  done

  # Upload the updated index.html to S3
  aws s3 --endpoint-url="${s3_endpoint_url}" cp index.html "s3://${s3_bucket}/"

  # Clean up
  rm -rf vm-output index.html
}

# Main script execution starts here

# Navigate to the script directory and execute make-boot-image.sh
cd build/appliance/build/kvm/cloud-init || exit
bash make-boot-image.sh

# Navigate back and run packer commands
cd .. || exit

# Check if glaber apt is available
curl -s "https://glaber.io/${glaber_repo}/debian/pool/main/g/glaber/" | \
        grep debian11 | \
        grep glaber-server-mysql_${glaber_build_version} || exit 1

# Set correct configs for packer
ln -s ../../../../build/appliance/run/ansible ansible
sed -i 's/hosts: glaber/hosts: default/' ansible/glaber.yaml
rm ansible/inventory/hosts.ini
sed -i '/^inventory = \.\/inventory$/d' ansible/ansible.cfg
sed -i '/remote_user:\ root/d' ansible/glaber.yaml

export glaber_db_type="postgres"
packer init .
packer validate .
packer build -color=false -timestamp-ui -warn-on-undeclared-var .

# Upload to S3 and update index.html for Postgres
upload_to_s3_and_update_index "postgres"

# Change DB installation type to MySQL and build again
export glaber_db_type="mysql"
packer build -color=false -timestamp-ui -warn-on-undeclared-var .

# Upload to S3 and update index.html for MySQL
upload_to_s3_and_update_index "mysql"
