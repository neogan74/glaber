/*
** Copyright Glaber 2018-2023
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

#include "zbxcommon.h"
#include "zbxalgo.h"
#include "../zbxcacheconfig/dbsync.h"
#include "../zbxcacheconfig/dbconfig.h"
#include "../../zabbix_server/glb_poller/poller_ipc.h"

//typedef struct {
//    u_int64_t orig_itemid;
//    u_int64_t conf_itemid;
//} ref_item_t;


typedef struct
{
	zbx_uint64_t		itemid;
	zbx_uint64_t		hostid;

	zbx_uint64_t		interfaceid;
	zbx_uint64_t		valuemapid;

	u_int16_t 		name;
	u_int16_t  		description;
	u_int16_t		key;
	
    u_int16_t		delay;
	u_int16_t		delay_ex;
	//u_int16_t		history_period;

	zbx_uint64_t		revision;

	unsigned char		poll_type;
    void                *poll_type_data;

	unsigned char		value_type;
    //u_int16_t           trend_period;
    unsigned char       has_trends_or_history;
	
	unsigned char		flags;
	unsigned char		admin_status;

	unsigned char		update_triggers;
	zbx_uint64_t		templateid;
	
	u_int16_t 			params;
	//future improvement: this should be "inventory name"
	//however it's quite pointless to do item->inventory link 
	//it's much more feasible to make cutstom attributes for the the host that
	//might refer to the lastvalues or event functions 
	unsigned char		inventory_link;

	//related objects
    //it's OK to convert this to just a text (json?)
	ZBX_DC_PREPROCITEM	*preproc_item;



    //it's feasible to keep dependancy relation out of the item_config object

	//ZBX_DC_MASTERITEM	*master_item; //not sure if needed at all, the reason we want to keep master->dep list
                                      //is to have it in the preprocessing


	ZBX_DC_TRIGGER		**triggers; //this is realation again?
	zbx_vector_ptr_t	tags;
	u_int64_t			master_itemid;
	
    //theese are related to queue mgmt, not the item config
	//probably, should be removed to queues and left in the dbcache
	int			data_expected_from;
	int			queue_next_check;
	unsigned char		location;
	unsigned char		queue_priority;
	unsigned char		poller_type;

    const char *serial_buffer;
}
conf_item_t;

typedef struct {
    zbx_hashset_t ref_items;
    elems_hash_t items;
    mem_funcs_t *memf;
} config_t;

static config_t *conf = {0};

void agent_clean_func(void *data) {

} 

void snmp_clean_func(void *data) {

}

void clean_item(conf_item_t *item) {
    switch (item->poll_type) {
        case ITEM_TYPE_AGENT:
            agent_clean_func(item->poll_type_data);
            break;
        case ITEM_TYPE_SNMP:
            snmp_clean_func(item->poll_type_data);
            break;
    }
}

ELEMS_CREATE(item_create_cb) {
    elem->data = memf->malloc_func(NULL, sizeof(conf_item_t));
    conf_item_t *item = (conf_item_t *)elem->data;

    bzero(elem->data, sizeof(conf_item_t));
    item->poll_type = ITEM_POLL_TYPE_NONE;
}

ELEMS_FREE(item_free_cb) {
    conf_item_t *item = (conf_item_t *)elem->data;
    clean_item(item);
}

void conf_items_init(mem_funcs_t *memf) {
    
    conf = (config_t*)memf->malloc_func(NULL, sizeof(config_t));
    conf->memf = memf;

    zbx_hashset_create_ext(&conf->ref_items, 1000, ZBX_DEFAULT_UINT64_HASH_FUNC, ZBX_DEFAULT_UINT64_COMPARE_FUNC, NULL, 
                memf->malloc_func, memf->realloc_func, memf->free_func);

    elems_hash_init(memf, item_create_cb, item_free_cb);

}


ELEMS_CALLBACK(item_update_cb) {
	return SUCCEED;
}
